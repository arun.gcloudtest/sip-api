from flask import Flask, render_template, jsonify, request, make_response
from os import path

app = Flask(__name__)


@app.errorhandler(404)
def handle_404_error(_error):
    _err = {
        "Input": request.json,
        "Output": "",
        "Status": 404,
        "Message": str(_error)
    }
    return make_response(jsonify(_err), 404)


@app.errorhandler(400)
def handle_400_error(_error):
    """Return a http 400 error to client"""
    _err = {
        "Input": request.json,
        "Output": "",
        "Status": 400,
        "Message": str(_error)
    }
    return make_response(jsonify(_err), 400)


@app.errorhandler(401)
def handle_401_error(_error):
    """Return a http 401 error to client"""
    _err = {
        "Input": request.json,
        "Output": "",
        "Status": 401,
        "Message": str(_error)
    }
    return make_response(jsonify(_err), 401)


@app.errorhandler(500)
def handle_500_error(_error):
    """Return a http 500 error to client"""
    _err = {
        "Input": request.json,
        "Output": "",
        "Status": 500,
        "Message": str(_error)
    }
    return make_response(jsonify(_err), 500)


@app.route('/api/encrypt', methods=['POST', 'GET'])
def encrypt():
    """
    This func will encrypt the given API
    :return: Payload and Status Code
    """

    resp = {"Input": None, "Output": None, "Status": "Failed", "Message": str(e)}
    return make_response(jsonify(resp), 400)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
